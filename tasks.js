// 1
const matches = (obj, source) =>
  Object.keys(source).every(key => obj.hasOwnProperty(key) && obj[key] === source[key])
console.log(matches({ age: 25, hair: 'long', beard: true }, { hair: 'long', beard: true }))

// 2
const larger = (first, second) =>  {
    return Math.max(first, second)
}
console.log(larger(5, 3))

// 3
const is_array = (input) => {
    if (toString.call(input) === "[object Array]") {
        return true
    }
    return false
}
console.log(is_array('resource'))
console.log(is_array([1, 2, 4, 0]))

// 4
const first = (array, n) => {
    if (array == null)
        return void 0
    if (n == null)
        return array[0]
    if (n < 0)
        return []
    return array.slice(0, n)
}
console.log(first([7, 9, 0, -2]))
console.log(first([], 5))
console.log(first([7, 9, 0, -2], 2))
console.log(first([7, 9, 0, -2], 4))
console.log(first([7, 9, 0, -2], -3))

// 5
const currentDay = () => {
    return new Date().toDateString()
}
console.log(currentDay())

// 6
// 'alert' is almost same as 'console.log' (alert doesn't work in node)
const x = 7
const y = -5
const z = 4
if (x > y && x > z) {
    if (y > z) {
        console.log(x + ", " + y + ", " + z)
    } else {
        console.log(x + ", " + z + ", " + y)
    }
}
else if (y > x && y > z) {
    if (x > z) {
        console.log(y + ", " + x + ", " + z)
    } else {
        console.log(y + ", " + z + ", " + x)
    }
}
else if (z > x && z > y) {
    if ( x > y) {
        console.log(z + ", " + x + ", " + y)
    } else {
        console.log(z + ", " + y + ", " + x)
    }
}  

// 7
const dig = (obj, target) =>
  target in obj
    ? obj[target]
    : Object.values(obj).reduce((acc, val) => {
        if (acc !== undefined) return acc
        if (typeof val === 'object') return dig(val, target)
    }, undefined)
const data = {
  level1: {
    level2: {
      level3: 'some data'
    }
  }
}
const dog = {
    "status": "success",
    "message": "https://images.dog.ceo/breeds/african/n02116738_1105.jpg"
}
console.log(dig(data, 'level3'))
console.log(dig(data, 'level4'))
console.log(dig(dog, 'message'))

// 8
const getRandomColor = () => {
    const letters = "0123456789ABCDEF"
    let color = '#'
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)]
    } 
    return color
}
console.log(getRandomColor())

// 9
const remove_non_ascii = (str) => {
    if ((str === null) || (str === ''))
        return false
    else
        str = str.toString()
    return str.replace(/[^\x20-\x7E]/g, '')
}
console.log(remove_non_ascii('äÄçÇéÉêPHP-MySQLöÖÐþúÚ'))

